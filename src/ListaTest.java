import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ListaTest {

    @Test
    public void testDuplicateName() {
        String str = "lista orase (3)";
        assertEquals("lista orase (4)", Lista.duplicateName(str, 4));
    }

    @Test
    public void testCountNumberOfApparitions() {
        List<String> lista = new ArrayList<>(Arrays.asList("nume orase", "nume orase (1)", "nume orase (2)", "nume tari europene"));
        String name = "nume orase";
        assertEquals(Integer.valueOf(3), Lista.countNumberOfApparitions(name, lista));
    }

    @Test
    public void testCountAndDuplicate() {
        List<String> lista = new ArrayList<>(Arrays.asList(
                "nume orase",
                "nume orase (1)",
                "nume orase (2)",
                "nume tari europene",
                "nume tari europene (1)"
        )
        );


        String str = "nume tari europene (1)";

        assertEquals("nume tari europene (2)",Lista.duplicateName(str, Lista.countNumberOfApparitions(str,lista)));
    }

    @Test
    public void testCreateNewListItem(){
        List<String> beforeList = new ArrayList<>(Arrays.asList(
                "nume orase",
                "nume orase (1)",
                "nume orase (2)",
                "nume tari europene",
                "nume tari europene (1)"
        )
        );

        List<String> afterList = new ArrayList<>(Arrays.asList(
                "nume orase",
                "nume orase (1)",
                "nume orase (2)",
                "nume tari europene",
                "nume tari europene (1)",
                "nume tari europene (2)"
                )
        );

        String name = "nume tari europene (1)";
        assertEquals(afterList, Lista.createNewListItem(beforeList,name));
    }

    @Test
    public void testIsNameMoreThanOnceAndDuplicate(){
        assertTrue(Lista.isNameMoreThanOnceAndDuplicate("blabla(", 2));
    }

    @Test
    public void testReplaceIndexInName(){
        assertEquals("nume orase (2)", Lista.replaceIndexInName("nume orase (1)", 2));
    }

    @Test
    public void testTrimName(){
        assertEquals("nume orase",Lista.trimName("nume orase (4)"));
    }
}