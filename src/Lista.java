import java.util.List;
import java.util.stream.Collectors;

public class Lista {

    static boolean isNameMoreThanOnceAndDuplicate(String name, Integer numberOfApparitions){
        return numberOfApparitions > 1 && name.contains("(");
    }

    static String replaceIndexInName(String name, Integer numberOfApparitions){
        return name.replace(name.charAt(name.length() - 2), numberOfApparitions.toString().charAt(0));
    }

    public static String duplicateName(String name, Integer numberOfApparitions) {
        if (isNameMoreThanOnceAndDuplicate(name,numberOfApparitions))
            return replaceIndexInName(name,numberOfApparitions);

        else
            return name + " (" + numberOfApparitions + ")";

    }

    static String trimName (String name){
        if (name.contains("("))
            return name.substring(0, name.length() - 4);
        else return name;
    }

    public static Integer countNumberOfApparitions(String name, List<String> list) {

        name = trimName(name);
        String finalName = name;
        return list.stream().filter(x -> x.contains(finalName)).collect(Collectors.toList()).size();

    }

    public static List<String> createNewListItem (List<String> list, String name){
        list.add(duplicateName(name, countNumberOfApparitions(name, list)));
        return list;
    }

}
